#include <stdio.h>

int main( void )
{
  int cur, prev;
  int ind = 1;
  int exists = 0;

  scanf( "%d", &cur );
  while (1)
    {
      prev = cur;
      scanf( "%d", &cur );

      if (cur == 9999) break;

      if (cur%2 == 0 && prev%2 == 0) {
        exists = 1;
        printf( "There are 2 next numbers with indexies %d %d\n", ind, ind + 1 );
        break;
      }
      ind++;
    }

  if (!exists)
    printf( "All numbers in sequense were different.\n" );

  return 0;
}
